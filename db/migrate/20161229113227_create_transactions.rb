class CreateTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :transactions do |t|
      t.belongs_to :from_account, :account
      t.belongs_to :to_account, :account
      t.integer :amount_subunit, null: false
      t.string :currency, null: false
      t.string :name
      t.text :description
      t.date :date, null: false

      t.timestamps
    end
  end
end

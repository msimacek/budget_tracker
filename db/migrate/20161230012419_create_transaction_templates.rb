class CreateTransactionTemplates < ActiveRecord::Migration[5.0]
  def change
    create_table :transaction_templates do |t|
      t.integer :tx_type, null: false
      t.string :template_name, null: false
      t.belongs_to :from_account
      t.belongs_to :to_account
      t.integer :amount_subunit
      t.string :currency
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end

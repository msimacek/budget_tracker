class AddFieldsToTransactionTemplate < ActiveRecord::Migration[5.0]
  def change
    add_reference :transaction_templates, :category
    add_column :transaction_templates, :tag_list, :string
  end
end

class CreateAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts do |t|
      t.belongs_to :user
      t.string :name, null: false
      t.text :description, null: false
      t.integer :balance_subunit, null: false, default: 0
      t.string :currency, null: false

      t.timestamps
    end
    add_index :accounts, :name, unique: true
  end
end

class TagTransactionRelation < ActiveRecord::Migration[5.0]
  def change
    create_table :transactions_tags, id: false do |t|
      t.belongs_to :transaction, index: true
      t.belongs_to :tag, index: true
    end
  end
end

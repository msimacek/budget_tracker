class AddCategoryToTransaction < ActiveRecord::Migration[5.0]
  def change
    add_reference :transactions, :category
  end
end

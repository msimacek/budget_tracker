# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170205211855) do

  create_table "accounts", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name",                        null: false
    t.text     "description",                 null: false
    t.integer  "balance_subunit", default: 0, null: false
    t.string   "currency",                    null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["name"], name: "index_accounts_on_name", unique: true
    t.index ["user_id"], name: "index_accounts_on_user_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transaction_templates", force: :cascade do |t|
    t.integer  "tx_type",         null: false
    t.string   "template_name",   null: false
    t.integer  "from_account_id"
    t.integer  "to_account_id"
    t.integer  "amount_subunit"
    t.string   "currency"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "category_id"
    t.string   "tag_list"
    t.index ["category_id"], name: "index_transaction_templates_on_category_id"
    t.index ["from_account_id"], name: "index_transaction_templates_on_from_account_id"
    t.index ["to_account_id"], name: "index_transaction_templates_on_to_account_id"
  end

  create_table "transactions", force: :cascade do |t|
    t.integer  "from_account_id"
    t.integer  "account_id"
    t.integer  "to_account_id"
    t.integer  "amount_subunit",  null: false
    t.string   "currency",        null: false
    t.string   "name"
    t.text     "description"
    t.date     "date",            null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "category_id"
    t.index ["account_id"], name: "index_transactions_on_account_id"
    t.index ["category_id"], name: "index_transactions_on_category_id"
    t.index ["from_account_id"], name: "index_transactions_on_from_account_id"
    t.index ["to_account_id"], name: "index_transactions_on_to_account_id"
  end

  create_table "transactions_tags", id: false, force: :cascade do |t|
    t.integer "transaction_id"
    t.integer "tag_id"
    t.index ["tag_id"], name: "index_transactions_tags_on_tag_id"
    t.index ["transaction_id"], name: "index_transactions_tags_on_transaction_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end

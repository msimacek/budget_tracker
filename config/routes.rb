Rails.application.routes.draw do
  authenticated :user do
    root to: 'accounts#index', as: :authenticated_root
  end
  root to: redirect('/login')
  resources :accounts
  devise_for :users, path: '', path_names: {
    sign_in: 'login', sign_out: 'logout'
  }
  resources :categories
  resources :tags
  resources :transaction_templates
  resources :transactions
end

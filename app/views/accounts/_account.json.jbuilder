json.extract! account, :id, :user_id, :name, :description, :balance_subunit, :currency, :created_at, :updated_at
json.url account_url(account, format: :json)
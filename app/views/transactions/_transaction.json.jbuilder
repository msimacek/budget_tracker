json.extract! transaction, :id, :from_account_id, :to_account_id, :balance_subunit, :name, :description, :date, :created_at, :updated_at
json.url transaction_url(transaction, format: :json)
class TransactionTemplatesController < ApplicationController
  before_action :set_transaction_template, only: [:show, :edit, :update, :destroy]

  # GET /transaction_templates
  # GET /transaction_templates.json
  def index
    @templates = TransactionTemplate.all
  end

  # GET /transaction_templates/1
  # GET /transaction_templates/1.json
  def show
  end

  # GET /transaction_templates/new
  def new
    @template = TransactionTemplate.new
    @template.tx_type = params[:type]
    respond_modal(:new_template_modal, 'form', "Add #{@template.tx_type} template", @template)
  end

  # GET /transaction_templates/1/edit
  def edit
  end

  # POST /transaction_templates
  # POST /transaction_templates.json
  def create
    @template = TransactionTemplate.new(transaction_template_params)
    if @template.save
      respond_modal_redirect :transaction_templates
    else
      respond_modal(
        :new_template_modal,
        'form',
        "Add #{@template.tx_type} template",
        @template
      )
    end
  end

  # PATCH/PUT /transaction_templates/1
  # PATCH/PUT /transaction_templates/1.json
  def update
    respond_to do |format|
      if @template.update(transaction_template_params)
        format.html { redirect_to @template, notice: 'Transaction template was successfully updated.' }
        format.json { render :show, status: :ok, location: @template }
      else
        format.html { render :edit }
        format.json { render json: @template.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transaction_templates/1
  # DELETE /transaction_templates/1.json
  def destroy
    @template.destroy
    respond_to do |format|
      format.html { redirect_to transaction_templates_url, notice: 'Transaction template was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction_template
      @template = TransactionTemplate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_template_params
      params.require(:transaction_template).permit(:tx_type, :template_name, :from_account_id, :to_account_id, :amount, :currency, :name, :description, :tag_list, :category_id)
    end
end

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :authenticate_user!

  def respond_modal(id, partial, title, object)
    @id = id
    @partial = partial
    @title = title
    @object = object
    respond_to do |format|
      format.html { render 'application/modal' }
      format.js { render 'application/modal' }
    end
  end

  def respond_modal_redirect(*args)
    url = request.env['HTTP_REFERER'] || url_for(*args)
    respond_to do |format|
      format.html { redirect_to(url) }
      format.js { render js: "window.location='#{url}'" }
    end
  end
end

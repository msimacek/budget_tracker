class TransactionsController < ApplicationController
  before_action :set_transaction, only: [:show, :edit, :update, :destroy]
  before_action :set_template, only: [:new, :create]
  before_action :set_filter, only: [:index]

  # GET /transactions
  # GET /transactions.json
  def index
    if filter_params
      @transactions = Transaction.all
      apply_condition(
        "category_id = :id",
        id: @filter.category_id
      )
      apply_condition(
        "from_account_id = :id OR to_account_id = :id",
        id: @filter.account
      )
      apply_condition("date >= ?", @filter.from_date)
      apply_condition("date <= ?", @filter.to_date)
      apply_condition(
        "EXISTS (
           SELECT 1
             FROM transactions_tags
               INNER JOIN tags
                 ON tags.id = transactions_tags.tag_id
           WHERE transaction_id = transactions.id
             AND tags.name IN (?)
         )",
        @filter.tags
      )
      @transactions = case @filter.type
                     when 'expense'
                       @transactions.where(to_account_id: nil)
                     when 'income'
                       @transactions.where(from_account_id: nil)
                     when 'transfer'
                       @transactions.where(
                         'from_account_id IS NOT NULL AND to_account_id IS NOT NULL'
                       )
                     else
                       @transactions
                     end
    end
  end

  # GET /transactions/1
  # GET /transactions/1.json
  def show
  end

  # GET /transactions/new
  def new
    @transaction = @template.new_transaction
    @transaction.from_account_id = params[:from_account] if params[:from_account]
    @transaction.to_account_id = params[:to_account] if params[:to_account]
    @transaction.currency = params[:currency] if params[:currency]
    @transaction.date = Date.today
    respond_modal(:new_transaction_modal, 'form', "Add #{@template.template_name}", @transaction)
  end

  # GET /transactions/1/edit
  def edit
    respond_modal(:update_transaction_modal, 'form', "Edit #{@tx_type}", @transaction)
  end

  # POST /transactions
  # POST /transactions.json
  def create
    @transaction = Transaction.new(transaction_params)
    if @transaction.save
      respond_modal_redirect :transactions
    else
      respond_modal(
        :new_transaction_modal,
        'form',
        "Add #{@template.template_name}",
        @transaction
      )
    end
  end

  # PATCH/PUT /transactions/1
  # PATCH/PUT /transactions/1.json
  def update
    if @transaction.update(transaction_params)
      respond_modal_redirect :transactions
    else
      respond_modal(
        :update_transaction_modal,
        'form',
        "Edit #{@tx_type}",
        @transaction
      )
    end
  end

  # DELETE /transactions/1
  # DELETE /transactions/1.json
  def destroy
    @transaction.destroy
    respond_to do |format|
      format.html { redirect_to transactions_url, notice: 'Transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  def set_transaction
    @transaction = Transaction.find(params[:id])
    @tx_type = @transaction.tx_type
  end

  def set_template
    @template = case params[:template]
                when nil, 'transfer'
                  TransactionTemplate.new(tx_type: :transfer)
                when 'expense'
                  TransactionTemplate.new(tx_type: :expense)
                when 'income'
                  TransactionTemplate.new(tx_type: :income)
                else
                  TransactionTemplate.find_by_template_name(params[:template])
                end
    @template.template_name ||= @template.tx_type.to_s
    @tx_type = @template.tx_type
  end

  def transaction_params
    params.require(:transaction).permit(:from_account_id, :to_account_id, :amount, :currency, :name, :description, :date, :category_id, :tag_list)
  end

  def set_filter
    @filter = Filter.new(filter_params)
    @format = params[:format]
  end

  def filter_params
    if params[:filter]
      params[:filter].permit(:category_id, :from_date, :to_date, :tag_list, :account, :type)
    end
  end

  def apply_condition(expression, *values, **key_values)
    if values.all?(&:present?) and key_values.values.all?(&:present?)
      values << key_values unless key_values.empty?
      @transactions = @transactions.where(expression, *values)
    end
  end
end

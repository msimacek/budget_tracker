class AccountsController < ApplicationController
  before_action :set_account, only: [:show, :edit, :update, :destroy]

  # GET /accounts
  # GET /accounts.json
  def index
    @accounts = Account.all
  end

  # GET /accounts/1
  # GET /accounts/1.json
  def show
  end

  # GET /accounts/new
  def new
    @account = Account.new
    respond_modal(:new_account_modal, 'form', "New account", @account)
  end

  # GET /accounts/1/edit
  def edit
    respond_modal(:update_account_modal, 'form', "Edit account", @account)
  end

  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new(account_params)
    @account.user_id = current_user.id

    if @account.save
      respond_modal_redirect :accounts
    else
      respond_modal(:new_account_modal, 'form', "New account", @account)
    end
  end

  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    if @account.update(account_params)
      respond_modal_redirect :accounts
    else
      respond_modal(:update_account_modal, 'form', "Edit account", @account)
    end
  end

  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    @account.destroy
    respond_to do |format|
      format.html { redirect_to accounts_url, notice: 'Account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_account
    @account = Account.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def account_params
    params.require(:account).permit(:name, :description, :balance, :currency)
  end
end

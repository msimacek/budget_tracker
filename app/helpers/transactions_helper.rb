module TransactionsHelper
  def transaction_pie_chart(transactions)
    data = transactions.to_a.group_by(&:category).collect do |cat, txs|
      [cat ? cat.name : 'uncategorized',
       txs.sum(&:amount).exchange_to(:CZK).to_f]
    end
    pie_chart(data)
  end
end

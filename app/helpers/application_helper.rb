module ApplicationHelper
  class BootstrapForm::FormBuilder
    def datepicker(name)
      form_group(name, label: {}) do
        concat(
          @template.content_tag(
            :div,
            data: {
              provide: 'datepicker',
              date_format: 'yyyy-mm-dd',
              date_autoclose: true,
              date_clear_btn: true,
            },
            class: 'input-group date',
          ) do
            concat(
              text_field_without_bootstrap(
                name,
                class: 'form-control bootstrap-datepicker',
                readonly: true,
              )
            )
            concat(
              @template.content_tag(
                :span,
                class: 'input-group-addon',
              ) do
                concat(
                  @template.content_tag(
                    :span,
                    '',
                    class: 'fa fa-calendar',
                  )
                )
              end
            )
          end
        )
      end
    end
  end
end

class TransactionTemplate < ApplicationRecord
  belongs_to :from_account, class_name: 'Account', optional: true
  belongs_to :to_account, class_name: 'Account', optional: true
  belongs_to :category, optional: true

  monetize :amount_subunit, as: 'amount',
                            with_model_currency: :currency,
                            allow_nil: true

  validates :template_name, presence: true

  enum tx_type: [:expense, :income, :transfer]

  def new_transaction
    Transaction.new(attributes.except('tx_type', 'created_at', 'updated_at', 'template_name'))
  end
end

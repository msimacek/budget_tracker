class Transaction < ApplicationRecord
  belongs_to :from_account, class_name: 'Account', optional: true
  belongs_to :to_account, class_name: 'Account', optional: true
  belongs_to :category, optional: true
  has_many :transactions_tags, inverse_of: :tx
  has_many :tags, through: :transactions_tags, source: :tag

  monetize :amount_subunit, as: 'amount',
                            with_model_currency: :currency

  validates :amount, numericality: true
  validates :currency, presence: true
  validates :date, presence: true

  validate :validate_account

  after_save :update_accounts
  after_destroy :update_accounts

  def tag_list
    tags.map(&:name).join(' ')
  end

  def tag_list=(list)
    self.tags = list.split(/ +/).collect { |tag| Tag.find_or_create_by(name: tag) } if list
  end

  def has_description?
    description.blank?
  end

  def tx_type
    if from_account.nil?
      :income
    elsif to_account.nil?
      :expense
    else
      :transfer
    end
  end

  private

  def update_accounts
    accounts = {}
    accounts[from_account.id] = from_account if from_account
    accounts[to_account.id] = to_account if to_account
    prev_from_account, prev_to_account =
      [from_account_id_was, to_account_id_was].collect do |id|
        if id.nil?
          nil
        elsif accounts[id]
          accounts[id]
        else
          accounts[id] = Account.find(id)
        end
      end
    prev_amount = Money.new(amount_subunit_was || 0, currency_was)
    curr_amount = Money.new(if destroyed? then 0 else amount_subunit end, currency)
    [
      [prev_from_account, prev_amount],
      [prev_to_account, -prev_amount],
      [from_account, -curr_amount],
      [to_account, curr_amount]
    ].each do |account, amount|
      account.balance += amount if account && amount != 0
    end
    accounts.values.collect(&:save).all?
  end

  def validate_account
    if from_account.nil? and to_account.nil?
      errors.add(:base, 'Needs to have account set')
    elsif from_account == to_account
      errors.add(:base, 'Accounts must not be the same')
    end
  end
end

class Filter
  include ActiveModel::Model

  attr_accessor :category_id, :account, :tags, :from_date, :to_date, :type

  def tag_list
    tags.join(' ') if tags
  end

  def tag_list=(list)
    self.tags = list.split(/ +/)
  end

end

class Account < ApplicationRecord
  belongs_to :user

  monetize :balance_subunit, as: 'balance',
                             with_model_currency: :currency

  validates :name, presence: true, uniqueness: true
  validates :description, presence: true, allow_blank: true
  validates :balance, presence: true, numericality: true
  validates :currency, presence: true

  def transactions
    Transaction.all.where('from_account_id = :id OR to_account_id = :id',
                          id: id)
  end

  private
end

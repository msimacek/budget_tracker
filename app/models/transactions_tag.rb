class TransactionsTag < ApplicationRecord
  belongs_to :tx, class_name: 'Transaction'
  belongs_to :tag
end

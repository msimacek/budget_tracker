require 'test_helper'

class TransactionTemplatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @transaction_template = transaction_templates(:one)
  end

  test "should get index" do
    get transaction_templates_url
    assert_response :success
  end

  test "should get new" do
    get new_transaction_template_url
    assert_response :success
  end

  test "should create transaction_template" do
    assert_difference('TransactionTemplate.count') do
      post transaction_templates_url, params: { transaction_template: { amount_subunit: @transaction_template.amount_subunit, currency: @transaction_template.currency, description: @transaction_template.description, from_account_id: @transaction_template.from_account_id, name: @transaction_template.name, template_name: @transaction_template.template_name, to_account_id: @transaction_template.to_account_id, tx_type: @transaction_template.tx_type } }
    end

    assert_redirected_to transaction_template_url(TransactionTemplate.last)
  end

  test "should show transaction_template" do
    get transaction_template_url(@transaction_template)
    assert_response :success
  end

  test "should get edit" do
    get edit_transaction_template_url(@transaction_template)
    assert_response :success
  end

  test "should update transaction_template" do
    patch transaction_template_url(@transaction_template), params: { transaction_template: { amount_subunit: @transaction_template.amount_subunit, currency: @transaction_template.currency, description: @transaction_template.description, from_account_id: @transaction_template.from_account_id, name: @transaction_template.name, template_name: @transaction_template.template_name, to_account_id: @transaction_template.to_account_id, tx_type: @transaction_template.tx_type } }
    assert_redirected_to transaction_template_url(@transaction_template)
  end

  test "should destroy transaction_template" do
    assert_difference('TransactionTemplate.count', -1) do
      delete transaction_template_url(@transaction_template)
    end

    assert_redirected_to transaction_templates_url
  end
end

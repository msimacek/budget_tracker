require 'test_helper'

class TransactionTest < ActiveSupport::TestCase
  self.use_instantiated_fixtures = true

  test 'transaction creation updates account balance' do
    tx = Transaction.create!(name: 'exp', from_account: @bank, to_account: @wallet,
                             amount: 100, date: Date.today, currency: 'CZK')
    assert_equal Money.new(90000, :CZK), @bank.balance
    assert_equal Money.new(20000, :CZK), @wallet.balance
  end

  test 'transaction update updates account balance' do
    tx = Transaction.create!(name: 'exp', from_account: @bank, amount: 100,
                             date: Date.today, currency: 'CZK')
    tx.update!(amount: 150)
    tx.update!(amount: 200)
    assert_equal Money.new(80000, :CZK), @bank.balance
  end

  test 'transaction account swap updates account balance' do
    tx = Transaction.create!(name: 'exp', from_account: @bank, to_account: @wallet,
                             amount: 100, date: Date.today, currency: 'CZK')
    tx.update!(from_account: @wallet, to_account: @bank)
    assert_equal Money.new(110000, :CZK), @bank.balance
    assert_equal Money.new(0, :CZK), @wallet.balance
  end

  test 'transaction delete updates account balance' do
    tx = Transaction.create!(name: 'exp', from_account: @bank, to_account: @wallet,
                             amount: 100, date: Date.today, currency: 'CZK')
    tx.destroy!
    assert_equal Money.new(100000, :CZK), @bank.balance
    assert_equal Money.new(10000, :CZK), @wallet.balance
  end

  test 'convert currency' do
    Money.add_rate(:CZK, :EUR, 1/25.0)
    Money.add_rate(:EUR, :CZK, 25)
    tx = Transaction.create!(name: 'exp', from_account: @bank,
                             amount: 1, date: Date.today, currency: 'EUR')
    assert_equal Money.new(97500, :CZK), @bank.balance
  end
end
